﻿use QLSach
go

------------------------------------------CUSTOMER--------------------------------------------------

-------------------------------------------------------------------------------
--  view_OrderHisoty: View get all order history
-------------------------------------------------------------------------------
if exists(select * from sys.views  where name = 'view_OrderHisoty' and type='v')
	drop view view_OrderHisoty
go
CREATE VIEW view_OrderHisoty
as
select h.MaHoaDon, makh, NgayMua
from hoadon as h INNER JOIN
                  ChiTietHoaDon as c ON h.MaHoaDon = c.MaHoaDon INNER JOIN
                  sach as s ON c.MaSach = s.masach
go

-------------------------------------------------------------------------------
--  fn_IsValidEmail: Function validate email
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'fn_IsValidEmail'))
	drop function fn_IsValidEmail
go

CREATE FUNCTION fn_IsValidEmail(@EMAIL varchar(100))RETURNS bit as
BEGIN     
  DECLARE @bitEmailVal as Bit
  DECLARE @EmailText varchar(100)

  SET @EmailText=ltrim(rtrim(isnull(@EMAIL,'')))

  SET @bitEmailVal = case when @EmailText = '' then 0
                          when @EmailText like '% %' then 0
                          when @EmailText like ('%["(),:;<>\]%') then 0
                          when substring(@EmailText,charindex('@',@EmailText),len(@EmailText)) like ('%[!#$%&*+/=?^`_{|]%') then 0
                          when (left(@EmailText,1) like ('[-_.+]') or right(@EmailText,1) like ('[-_.+]')) then 0                                                                                    
                          when (@EmailText like '%[%' or @EmailText like '%]%') then 0
                          when @EmailText LIKE '%@%@%' then 0
                          when @EmailText NOT LIKE '_%@_%._%' then 0
                          else 1 
                      end
  RETURN @bitEmailVal
END 
GO



-------------------------------------------------------------------------------
--  proc_KhachHang_Register: Đăng ký một tài khoản khách hàng mới
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_KhachHang_Register'))
	drop procedure proc_KhachHang_Register
go
create procedure proc_KhachHang_Register
@hoten nvarchar(255),
@tendn varchar(50),
@pass varchar(50),
@diachi nvarchar(455),
@email nvarchar(50),
@sodt nvarchar(15),
@makh int output

as
begin
	set nocount on;
		if((@tendn is null) or @tendn = N'')
			begin
				set @makh = -3
				return;
			end
		if(exists(select * from KhachHang as t where t.tendn = @tendn))
			begin
				set @makh = -5
				return;
			end
		if(@pass = N'' or LEN(@pass) < 1)
			begin
				set @makh = -2
				return;
			end
		if ([dbo].[fn_IsValidEmail](@email) = 0)
			begin
				set @makh = -1
				return;
			end
		if(@sodt = NULL or LEN(@sodt) < 1)
			begin
				set @makh = -4
				return;
			end

		insert into KhachHang(hoten, tendn, pass, email, diachi, sodt)
		values (@hoten, @tendn, @pass,Lower( @email), @diachi, @sodt);
		set @makh = SCOPE_IDENTITY();
end
go

-- Test thủ tục: proc_KhachHang_Register
--declare @makh int 
--exec  proc_KhachHang_Register		
--@hoten = 'a',
--@tendn = '1fasd232',
--@pass = '12323',
--@diachi = '32 a',
--@email = 'kjda@gmail.com',
--@sodt = '09302',
--@makh = @makh output



-------------------------------------------------------------------------------
--  proc_KhachHang_Authenticate: Kiểm tra đang nhập khách hàng
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_KhachHang_Authenticate'))
	drop procedure proc_KhachHang_Authenticate
go
create procedure proc_KhachHang_Authenticate
@tendn varchar(255),
@pass varchar(255) 
as
begin
	set nocount on;
	select top 1 * from KhachHang as t where t.tendn = @tendn and t.pass = @pass

end
go

-- Test thủ tục: proc_KhachHang_Authenticate
--exec  proc_KhachHang_Authenticate
--		@tendn = 'kh1',
--		@pass = 123
		

-------------------------------------------------------------------------------
--  proc_HoaDon_Create: Tạo mới một hoá đơn
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_HoaDon_Create'))
	drop procedure proc_HoaDon_Create
go
create procedure proc_HoaDon_Create
@maKh bigint,
@ngayMua date,
@damua bit,
@maHoaDon int output
as
begin
	set nocount on;
	if(not exists(select * from KhachHang as t where t.makh = @maKh))
		begin
			set @maHoaDon = -1
			return;
		end
	insert into HoaDon (makh, NgayMua, damua) values(@maKh, @ngayMua, @damua)
	set @maHoaDon = @@IDENTITY;
end
go

-- Test thủ tục: proc_HoaDon_Create
--declare @maHoaDon bigint;
--exec  proc_HoaDon_Create
--		@maKh = 52,
--		@ngayMua = '2021-02-02',
--		@daMua = 1,
--		@maHoaDon = @maHoaDon output

-------------------------------------------------------------------------------
--  proc_ChiTietHoaDon_Create: Tạo mới chi tiết hoá đơn
-------------------------------------------------------------------------------
if(exists(select * from sys.objects where name = 'proc_ChiTietHoaDon_Create'))
	drop procedure proc_ChiTietHoaDon_Create
go
create procedure proc_ChiTietHoaDon_Create
@maSach varchar(50),
@soLuong int,
@maHoaDon bigint,
@trangThai int,
@StatusCode int = 0 output
as
begin

	if(not exists(select * from hoadon as t where t.MaHoaDon = @maHoaDon))
		begin
			set @StatusCode = 0
			return;
		end

	set nocount on;
	insert into ChiTietHoaDon(MaSach, SoLuongMua, MaHoaDon, TrangThai)
	select @maSach, @soLuong, @maHoaDon, @trangThai
	where exists(select * from hoadon as h where h.MaHoaDon = @maHoaDon)
	if(@@ROWCOUNT > 0)
		set @StatusCode = 1
		return;
end
go

-- Test thủ tục: proc_ChiTietHoaDon_Create
--declare @StatusCode int;
--exec  proc_ChiTietHoaDon_Create
--		@maSach = '99',
--		@soLuong = 99,
--		@maHoaDon = 15,
--		@StatusCode = @StatusCode output

-------------------------------------------------------------------------------
--  proc_HoaDon_OrderHistory: Lấy lịch sử đặt hàng
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_HoaDon_OrderHistory'))
	drop procedure proc_HoaDon_OrderHistory
go
create procedure proc_HoaDon_OrderHistory
@customerId int
as
begin	
	set nocount on

	select s.gia, s.anh, tensach, c.SoLuongMua, h.NgayMua, c.TrangThai, h.MaHoaDon, c.MaSach
	from hoadon as h join ChiTietHoaDon as c on h.MaHoaDon = h.MaHoaDon 
		join sach as s on s.masach = c.MaSach
	where h.makh = @customerId
	order by h.NgayMua desc
	
end
go

--exec proc_HoaDon_OrderHistory
--@customerId = 23



-------------------------------------------------------------------------------
--- proc_Order_Tranforms_Cus: Thực hiện chức năng chuyển đổi trạng thái đơn hàng
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Tranforms_Cus')
	drop procedure proc_Order_Tranforms_Cus
go 
create procedure proc_Order_Tranforms_Cus
@maHoaDon nvarchar(255),
@trangThai int,
@Result bit output
as
begin
	set nocount on
	declare @status int

	UPDATE hoadon
	SET damua = 1
	WHERE MaHoaDon = @maHoaDon and damua = 0

	UPDATE ChiTietHoaDon
SET TrangThai = 
    CASE @trangThai
        WHEN 0	THEN 1
        WHEN 1 THEN 2
        ELSE -1
     END
	WHERE MaHoaDon = @maHoaDon


	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = -1
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_Order_Tranforms_Cus
--	@maHoaDon = '47',
--	@trangThai = 1,
--	@Result = @Result output;

--select @Result


-----------------------------------------------------------------------------
--- proc_Order_Cancel_Cus: Thực hiện chức năng huỷ đặt hàng
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Cancel_Cus')
	drop procedure proc_Order_Cancel_Cus
go 
create procedure proc_Order_Cancel_Cus
@maHoaDon nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete hoadon 
	where(MaHoaDon = @maHoaDon) and (exists(select * from ChiTietHoaDon where MaHoaDon = @maHoaDon ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_Order_Cancel_Cus
--	@maHoaDon = '47',
--	@Result = @Result output;

--select @Resul



------------------------------------------ADMIN--------------------------------------------------


-------------------------------------------------------------------------------
--  proc_Admin_Register: Đăng ký một tài khoản admin mới
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_Admin_Register'))
	drop procedure proc_Admin_Register
go
create procedure proc_Admin_Register
@tendn varchar(50),
@pass varchar(50),
@Result int output

as
begin
	set nocount on;
		if((@tendn is null) or @tendn = N'')
			begin
				set @Result = -3
				return;
			end
		if(exists(select * from DangNhap as t where t.TenDangNhap = @tendn))
			begin
				set @Result = -2
				return;
			end
		if(@pass = N'' or LEN(@pass) < 1)
			begin
				set @Result = -1
				return;
			end

		insert into DangNhap(TenDangNhap, MatKhau, Quyen)
		values (@tendn, @pass, 1);
		set @Result = 1;
end
go



-- Test thủ tục: proc_Admin_Register
--declare @Result int 
--exec  proc_Admin_Register		
--@tendn = 'admin',
--@pass = '12323',
--@Result = @Result output


-------------------------------------------------------------------------------
--  proc_Admin_Authenticate: Kiểm tra đang nhập Admin
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_Admin_Authenticate'))
	drop procedure proc_Admin_Authenticate
go
create procedure proc_Admin_Authenticate
@tendn varchar(255),
@pass varchar(255) 
as
begin
	set nocount on;
	select top 1 * from DangNhap as t where t.TenDangNhap = @tendn and t.MatKhau = @pass and t.Quyen = 1

end
go

 --Test thủ tục: proc_Admin_Authenticate
--exec  proc_Admin_Authenticate
--@tendn = 'admin',
--@pass = 'admin'
		

-------------------------------------------------------------------------------
--- proc_Book_Insert: thực hiện chức năng thêm mới sách
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Book_Insert')
	drop procedure proc_Book_Insert
go
create procedure proc_Book_Insert
@masach nvarchar(50),
@tensach nvarchar(50),
@soluong bigint,
@gia bigint,
@maloai nvarchar(50), 
@sotap nvarchar(50),
@anh nvarchar(50),
@NgayNhap nvarchar(50),
@tacgia nvarchar(50),
@Result int output

as
begin
	set nocount on
	
	if(@masach is null or @masach = N'' or @maloai is null or @tensach is null or @tensach = N'')
		begin
			set @Result = -2
			return;
		end
	if exists(select * from sach where masach  = @masach)
		begin
			set @Result = -1
			return;
		end

	Insert into sach (masach, maloai, tensach, soluong, gia, sotap, anh, NgayNhap, tacgia)
			values(@masach, @maloai, @tensach,@soluong, @gia, @sotap, @anh, @NgayNhap, @tacgia)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
go

-- Test thủ tục:
--declare @Result int;
--exec proc_Book_Insert
--@masach = 'b20',
--@tensach = N'Hạt Giống Tâm Hồn dành riêng cho phụ nữ',
--@soluong = 30,
--@gia = 22000,
--@maloai = N'Bi quyet cuoc song',
--@sotap = '2',
--@anh = 'image_sach/b20.jpg',
--@NgayNhap = '2021.11.15 10:04:06',
--@tacgia = 'Jack Canield-Mark Victor Hansen',
--@Result = @Result output

--select @Result


-------------------------------------------------------------------------------
--- proc_Book_Update: thực hiện chức năng cập nhật thông tin sách
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Book_Update')
	drop procedure proc_Book_Update
go
create procedure proc_Book_Update
@masach nvarchar(50),
@tensach nvarchar(50),
@soluong bigint,
@gia bigint,
@maloai nvarchar(50), 
@sotap nvarchar(50),
@anh nvarchar(50),
@NgayNhap nvarchar(50),
@tacgia nvarchar(50),
@Result bit output

as
begin
	set nocount on
	
	if(@masach is null or @masach = N'' or @maloai is null or @tensach is null or @tensach = N'')
		begin
			set @Result = 0
			return;
		end
	if not exists(select * from sach where masach  <> @masach)
		begin
			set @Result = 0
			return;
		end

	Update sach
	set maloai = @maloai,
		tensach = @tensach,
		soluong = @soluong,
		gia = @gia,
		sotap = @sotap,
		anh = @anh,
		NgayNhap = @NgayNhap,
		tacgia = @tacgia
	where masach = @masach


	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
go

-- Test thủ tục:
--declare @Result bit;
--exec proc_Book_Update
--@masach = 'b20',
--@tensach = N'Hạt Giống Tâm Hồn dành riêng cho phụ nữ',
--@soluong = 30,
--@gia = 22000,
--@maloai = N'Bi quyet cuoc song',
--@sotap = '2',
--@anh = 'image_sach/b20.jpg',
--@NgayNhap = '2021.11.15 10:04:06',
--@tacgia = 'Jack Canield-Mark Victor Hansen',
--@Result = @Result output

--select @Result

-------------------------------------------------------------------------------
--- proc_Book_Delete: thực hiện chức năng xoá sách
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Book_Delete')
	drop procedure proc_Book_Delete
go 
create procedure proc_Book_Delete
@maSach nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete sach 
	where(masach = @maSach) or (exists(select * from loai where masach = @maSach ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go
--Test thủ tục
--declare @Result bit;
--exec proc_Book_Delete
--	@maSach = '2',
--	@Result = @Result output;

--select @Result


-------------------------------------------------------------------------------
--- proc_TypesOfBooks_Insert: thực hiện chức năng thêm mới loại sách
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_TypesOfBooks_Insert')
	drop procedure proc_TypesOfBooks_Insert
go
create procedure proc_TypesOfBooks_Insert
@maLoai nvarchar(50),
@tenLoai nvarchar(250),
@Result int output

as
begin
	set nocount on
	
	if(@maLoai is null or @maLoai = N'' or @maloai is null or @tenLoai is null or @tenLoai = N'')
		begin
			set @Result = -2
			return;
		end
	if exists(select * from loai where maloai  = @maLoai)
		begin
			set @Result = -1
			return;
		end

	Insert into loai (maloai, tenloai) values(@maLoai, @tenLoai)

	if @@ROWCOUNT > 0
		set @Result = 1;
	else
		set @Result = -3;
end
go

-- Test thủ tục:
--declare @Result int;
--exec proc_TypesOfBooks_Insert
--@maLoai = 'test',
--@tenLoai = N'Test',
--@Result = @Result output

--select @Result


-------------------------------------------------------------------------------
--- proc_TypesOfBooks_Update: thực hiện chức năng cập nhật thông tin loại sách
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_TypesOfBooks_Update')
	drop procedure proc_TypesOfBooks_Update
go
create procedure proc_TypesOfBooks_Update
@maLoaiNew nvarchar(50),
@maLoaiOld nvarchar(50),
@tenLoai nvarchar(250),
@Result bit output

as
begin
	set nocount on
	
	if(@maLoaiOld is null or @maLoaiOld = N'' or @maLoaiNew is null or @maLoaiNew = N''
	 or @tenLoai is null or @tenLoai = N'')
		begin
			set @Result = 0
			return;
		end
	if not exists(select * from sach where maloai  <> @maLoaiOld)
		begin
			set @Result = 0
			return;
		end

	Update loai
	set maloai = @maLoaiNew,
		tenloai = @tenLoai
	where maloai = @maLoaiOld


	if(@@ROWCOUNT) > 0
		set @Result = 1;
	else
		set @Result = 0;
end
go

-- Test thủ tục:
--declare @Result bit;
--exec proc_TypesOfBooks_Update
--@maLoaiNew = 'Bi quyet cuoc song2',
--@maLoaiOld = 'Bi quyet cuoc song',
--@tenLoai = N'Bí quyết cuộc sống',
--@Result = @Result output

--select @Result


-------------------------------------------------------------------------------
--- proc_TypesOfBooks_Delete: thực hiện chức năng xoá loại sách
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_TypesOfBooks_Delete')
	drop procedure proc_TypesOfBooks_Delete
go 
create procedure proc_TypesOfBooks_Delete
@maLoai nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete loai 
	where(maloai = @maLoai) or (exists(select * from sach where maloai = @maLoai ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_TypesOfBooks_Delete
--	@maLoai = 'test',
--	@Result = @Result output;

--select @Result


-------------------------------------------------------------------------------
--  proc_Admin_GetAllOrderHistory: Lấy tất cả lịch sử đặt hàng
-------------------------------------------------------------------------------

if(exists(select * from sys.objects where name = 'proc_Admin_GetAllOrderHistory'))
	drop procedure proc_Admin_GetAllOrderHistory
go
create procedure proc_Admin_GetAllOrderHistory
as
begin	
	set nocount on

	select s.gia, s.anh, tensach, c.SoLuongMua, h.NgayMua, c.TrangThai, h.MaHoaDon, c.MaSach
	from hoadon as h join ChiTietHoaDon as c on h.MaHoaDon = h.MaHoaDon 
		join sach as s on s.masach = c.MaSach
	order by h.NgayMua desc
	
end
go

--exec proc_Admin_GetAllOrderHistory


-------------------------------------------------------------------------------
--- proc_Order_Delete: thực hiện chức năng xoá hoá đơn
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Delete')
	drop procedure proc_Order_Delete
go 
create procedure proc_Order_Delete
@maHoaDon nvarchar(255),
@Result bit output
as
begin
	set nocount on
	
	Delete hoadon 
	where(MaHoaDon = @maHoaDon) and (exists(select * from ChiTietHoaDon where MaHoaDon = @maHoaDon ))

	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = 0
end
go
--Test thủ tục
--declare @Result bit;
--exec proc_Order_Delete
--	@maHoaDon = '46',
--	@Result = @Result output;

--select @Result



-------------------------------------------------------------------------------
--- proc_Order_Tranforms: Thực hiện chức năng chuyển đổi trạng thái đơn hàng
-------------------------------------------------------------------------------

if exists(select * from sys.objects where name = 'proc_Order_Transforms')
	drop procedure proc_Order_Transforms
go 
create procedure proc_Order_Transforms
@maHoaDon nvarchar(255),
@trangThai int,
@Result bit output
as
begin
	set nocount on
	declare @status int
	UPDATE ChiTietHoaDon
SET TrangThai = 
    CASE @trangThai
        WHEN 0	THEN 1
        WHEN 1 THEN 2
        ELSE -1
     END
	WHERE MaHoaDon = @maHoaDon
	if (@@ROWCOUNT > 0)
		set @Result = 1
	else 
		set @Result = -1
end
go

--Test thủ tục
--declare @Result bit;
--exec proc_Order_Transforms
--	@maHoaDon = '38',
--	@trangThai = 0,
--	@Result = @Result output;

--select @Result


