package controller.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.AdminBean;
import model.bean.LoaiBean;
import model.bean.SachBean;
import model.bo.LoaiBo;
import model.bo.SachBo;

/**
 * Servlet implementation class ManageLoai
 */
@WebServlet("/manage-loai")
public class ManageLoai extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManageLoai() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	try {
	    request.setCharacterEncoding("UTF-8");
	    response.setCharacterEncoding("UTF-8");

	    SachBo sbo = new SachBo();
	    LoaiBo lbo = new LoaiBo();

	    ArrayList<SachBean> dssach = sbo.getSach();
	    ArrayList<LoaiBean> dsloai = lbo.getLoai();

	    String ml = request.getParameter("ml");
	    String key = request.getParameter("txttk");

	    if (ml != null)
		dssach = sbo.timMaLoai(ml);
	    else if (key != null) {
		dssach = sbo.timChung(key);
	    }

	    request.setAttribute("dsloai", dsloai);
	    request.setAttribute("timkiemsach", key);

	    HttpSession session = request.getSession();
	    AdminBean admin = (AdminBean) session.getAttribute("auth-admin");

	    if (admin == null) {
		request.setAttribute("isSignin", false);
		RequestDispatcher rd = request.getRequestDispatcher("views/admin/signin.jsp");
		rd.forward(request, response);
		return;
	    }
	    RequestDispatcher rd = request.getRequestDispatcher("views/admin/manage_loai.jsp");
	    rd.forward(request, response);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
