package controller.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bean.SachBean;
import model.bo.AdminBo;
import model.bo.LoaiBo;

/**
 * Servlet implementation class DeleteSach
 */
@WebServlet("/delete-sach")
public class DeleteSach extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteSach() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	try {

	    String maSach = request.getParameter("ms");

	    String tenSach = request.getParameter("ts");

	    String tacGia = request.getParameter("tg");
	    String maLoai = request.getParameter("ml");
	    String gia = request.getParameter("gia");
	    String anh = request.getParameter("anh");

	    String delMaSach = request.getParameter("delms");
	    
	    if (delMaSach == null) {
		Long giaBan = null;
		if (gia != null)
		    giaBan = Long.parseLong(gia);
		SachBean sb = new SachBean(maSach, tenSach, tacGia, giaBan, anh, maLoai);

		request.setAttribute("sachDelete", sb);
		LoaiBo lbo = new LoaiBo();

		ArrayList<LoaiBean> dsloai = lbo.getLoai();

		request.setAttribute("dsloai", dsloai);

		RequestDispatcher rd = request.getRequestDispatcher("views/admin/delete-sach.jsp");
		rd.forward(request, response);
		return;
	    }
	    AdminBo abo = new AdminBo();
	    boolean deleteSuccess = abo.kiemTraXS(delMaSach);
	    if (deleteSuccess == true) {
		request.setAttribute("deleteSuccess", delMaSach);
		RequestDispatcher rd = request.getRequestDispatcher("dashboard");
		rd.forward(request, response);
		return;
	    }
	    RequestDispatcher rd = request.getRequestDispatcher("views/admin/delete-sach.jsp");
	    rd.forward(request, response);
	    return;
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
