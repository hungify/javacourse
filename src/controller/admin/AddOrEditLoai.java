package controller.admin;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bo.AdminBo;

/**
 * Servlet implementation class AddOrEditLoai
 */
@WebServlet("/add-or-edit-loai")
public class AddOrEditLoai extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddOrEditLoai() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	try {
	    String maLoai = request.getParameter("ml");
	    String maLoaiOld = request.getParameter("ml-old");
	    String tenLoai = request.getParameter("tl");
	    String isAdd = request.getParameter("add");
	    String isEdit = request.getParameter("edit");

	    String add = request.getParameter("add");

	    String update = request.getParameter("update");

	    if (maLoai != null && tenLoai != null && add != null) {
		AdminBo adbo = new AdminBo();
		int isAddSuccess = adbo.kiemTraTL(maLoai, tenLoai);
		if (isAddSuccess == -1) {
		    LoaiBean existsMaLoai = new LoaiBean(maLoai, tenLoai);
		    request.setAttribute("existsMaLoai", existsMaLoai);
		    request.setAttribute("addOrEdit", isAdd);
		    RequestDispatcher rd = request.getRequestDispatcher("views/admin/add-or-edit-loai.jsp");
		    rd.forward(request, response);
		    return;
		}
		LoaiBean newLoai = new LoaiBean(maLoai, tenLoai);
		request.setAttribute("loaiAddNew", newLoai);
		RequestDispatcher rd = request.getRequestDispatcher("manage-loai");
		rd.forward(request, response);
		return;
	    } else if (maLoai != null && tenLoai != null && update != null) {

		AdminBo adbo = new AdminBo();

		boolean isEditSuccess = adbo.kiemTraCNL(maLoai, maLoaiOld, tenLoai);
		if (isEditSuccess == false) {
		    LoaiBean existsMaLoai = new LoaiBean(maLoai, tenLoai);
		    request.setAttribute("existsMaLoai", existsMaLoai);
		    request.setAttribute("addOrEdit", isEdit);
		    RequestDispatcher rd = request.getRequestDispatcher("views/admin/add-or-edit-loai.jsp");
		    rd.forward(request, response);
		    return;
		}
		LoaiBean newLoai = new LoaiBean(maLoai, tenLoai);
		request.setAttribute("loaiEditNew", newLoai);
		RequestDispatcher rd = request.getRequestDispatcher("manage-loai");
		rd.forward(request, response);
		return;
	    }

	    if (isEdit != null) {
		LoaiBean existsMaLoai = new LoaiBean(maLoai, tenLoai);
		request.setAttribute("existsMaLoai", existsMaLoai);
		request.setAttribute("addOrEdit", isEdit);
	    } else {
		request.setAttribute("addOrEdit", isAdd);
	    }

	    RequestDispatcher rd = request.getRequestDispatcher("views/admin/add-or-edit-loai.jsp");
	    rd.forward(request, response);

	} catch (

	Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}

