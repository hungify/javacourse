package controller.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.LoaiBean;
import model.bean.SachBean;
import model.bo.AdminBo;
import model.bo.LoaiBo;

/**
 * Servlet implementation class DeleteLoai
 */
@WebServlet("/delete-loai")
public class DeleteLoai extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteLoai() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");
	try {

	    String maLoai = request.getParameter("ml");

	    String tenLoai = request.getParameter("tl");

	    String delMaLoai = request.getParameter("delml");

	    if (delMaLoai == null) {
		LoaiBean lb = new LoaiBean(maLoai, tenLoai);

		request.setAttribute("loaiDelete", lb);
		LoaiBo lbo = new LoaiBo();

		ArrayList<LoaiBean> dsloai = lbo.getLoai();

		request.setAttribute("dsloai", dsloai);

		RequestDispatcher rd = request.getRequestDispatcher("views/admin/delete-loai.jsp");
		rd.forward(request, response);
		return;
	    }
	    AdminBo abo = new AdminBo();
	    boolean deleteSuccess = abo.kiemTraXL(delMaLoai);
	    if (deleteSuccess == true) {
		request.setAttribute("deleteSuccess", delMaLoai);
		RequestDispatcher rd = request.getRequestDispatcher("manage-loai");
		rd.forward(request, response);
		return;
	    }
	    RequestDispatcher rd = request.getRequestDispatcher("views/admin/delete-sach.jsp");
	    rd.forward(request, response);
	    return;
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
