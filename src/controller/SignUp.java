package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.KhachHangBean;
import model.bo.KhachHangBo;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/signup")
public class SignUp extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUp() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	request.setCharacterEncoding("UTF-8");
	response.setCharacterEncoding("UTF-8");

	String fullname = request.getParameter("fullname");
	String address = request.getParameter("address");
	String username = request.getParameter("username");
	String email = request.getParameter("email");
	String phoneNumber = request.getParameter("phone_number");
	String password = request.getParameter("password");

	String isNew = request.getParameter("new");
	try {

	    if (fullname != null && address != null && username != null && email != null && phoneNumber != null
		    && password != null) {

		String usernameLowerCase = username.toLowerCase();
		String emailLowerCase = email.toLowerCase();

		KhachHangBo khbo2 = new KhachHangBo();
		int isNewCustomer = khbo2.kiemTraDK(usernameLowerCase, fullname, emailLowerCase, password, address,
			phoneNumber);
		if (isNewCustomer > 0) {
		    KhachHangBean khb = new KhachHangBean(fullname, emailLowerCase, usernameLowerCase, address,
			    phoneNumber);
		    request.setAttribute("newCustomer", khb);
		    RequestDispatcher rd = request.getRequestDispatcher("signin");
		    rd.forward(request, response);
		    return;
		}
		KhachHangBean khbean = new KhachHangBean(username, fullname, emailLowerCase, address, phoneNumber);

		request.setAttribute("exsitsCustomer", khbean);
		RequestDispatcher rd = request.getRequestDispatcher("views/signup.jsp");
		rd.forward(request, response);
		return;
	    }
	    if (isNew != null) {
		request.setAttribute("isSignin", null);
	    }
	    RequestDispatcher rd = request.getRequestDispatcher("views/signup.jsp");
	    rd.forward(request, response);

	} catch (SQLException e) {
	    e.printStackTrace();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
