package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.KhachHangBean;
import model.bo.GioHangBo;

/**
 * Servlet implementation class PaymentController
 */
@WebServlet("/payment")
public class Payment extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Payment() {
	super();
	// TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	try {
	    request.setCharacterEncoding("UTF-8");
	    response.setCharacterEncoding("UTF-8");

	    HttpSession session = request.getSession();
	    KhachHangBean kh = (KhachHangBean) session.getAttribute("auth");
	    GioHangBo ghbo = (GioHangBo) session.getAttribute("giohang");

	    if (kh != null) {
		request.setAttribute("isSignin", true);
		RequestDispatcher rd = request.getRequestDispatcher("views/payment.jsp");
		rd.forward(request, response);
		return;
	    }
	    request.setAttribute("isSignin", false);
	    RequestDispatcher rd = request.getRequestDispatcher("home");
	    rd.forward(request, response);
	    return;
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	// TODO Auto-generated method stub
	doGet(request, response);
    }

}
