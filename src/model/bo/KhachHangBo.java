package model.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import model.bean.GioHangBean;
import model.bean.KhachHangBean;
import model.bean.LichSuMuaHangBean;
import model.dao.KhachHangDao;

public class KhachHangBo {
    KhachHangDao khdao = new KhachHangDao();

    public KhachHangBean kiemTraDN(String tendn, String pass) throws Exception {
	return khdao.kiemTraDangNhap(tendn, pass);
    }

    public int kiemTraDK(String username, String fullname, String email, String password, String address,
	    String phoneNumber) throws Exception {
	return khdao.kiemTraDangKy(username, fullname, email, password, address, phoneNumber);
    }

    public boolean datHang(int customerId, Date orderDate, int isBought, String bookId, int quantity)
	    throws SQLException, Exception {
	return khdao.datHang(customerId, orderDate, isBought, bookId, quantity);
    }

    public LichSuMuaHangBo layLichSuMuaHang(int customerId) throws Exception {
	return khdao.getOrderHistoty(customerId);
    }

    public int chuyenTrangThai(String orderId, String ttOrder) throws Exception {
	return khdao.chuyenTrangThaiDatHang(orderId, ttOrder);

    }

    public boolean huyDatHang(String orderId) throws Exception {
	return khdao.huyDatHang(orderId);
    }
}
