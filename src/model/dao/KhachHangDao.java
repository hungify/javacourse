package model.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.bean.GioHangBean;
import model.bean.KhachHangBean;
import model.bean.LichSuMuaHangBean;
import model.bean.SachBean;
import model.bo.LichSuMuaHangBo;

public class KhachHangDao {
    public KhachHangBean kiemTraDangNhap(String username, String password) throws Exception, SQLException {
	DungChung dc = new DungChung();
	dc.ketNoi();
	CallableStatement cs = dc.cn.prepareCall("{call proc_KhachHang_Authenticate(?, ?)}");

	cs.setString(1, username);
	cs.setString(2, password);

	ResultSet rs = cs.executeQuery();

	if (rs.next() == false) {
	    return null;
	} else {
	    KhachHangBean kh = new KhachHangBean();
	    kh.setMaKH(rs.getInt("makh"));
	    kh.setHoTen(rs.getString("hoten"));
	    kh.setTenDangNhap(rs.getString("tendn"));
	    kh.setDiaChi(rs.getString("diachi"));
	    kh.setEmail(rs.getString("email"));
	    kh.setSoDienThoai(rs.getString("sodt"));
	    return kh;
	}
    }

    public int kiemTraDangKy(String username, String fullname, String email, String password, String address,
	    String phoneNumber) throws Exception, SQLException {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_KhachHang_Register(?, ?, ?, ?, ?, ?, ?)}");

	cs.setString(1, fullname);
	cs.setString(2, username);
	cs.setString(3, password);
	cs.setString(4, address);
	cs.setString(5, email);
	cs.setString(6, phoneNumber);

	cs.registerOutParameter(7, Types.INTEGER);

	cs.execute();

	int newCustomerId = cs.getInt(7);

	if (newCustomerId == -1)
	    return -1;
	else if (newCustomerId == -2)
	    return -2;
	else if(newCustomerId == -3)
	    return -3;
	else if(newCustomerId == -4) 
	    return -4;
	else if(newCustomerId == -5)
	    return -5;
	else
	    return newCustomerId;

    }

    public boolean datHang(int customerId, Date orderDate, int isBought, String bookId, int quantity)
	    throws SQLException, Exception {

	DungChung dc = new DungChung();
	dc.ketNoi();

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");

	CallableStatement cstmt = dc.cn.prepareCall("{call proc_HoaDon_Create(?, ?, ?, ?)}");

	cstmt.setInt(1, customerId);
	cstmt.setString(2, sdf.format(orderDate));
	cstmt.setInt(3, isBought);
	cstmt.registerOutParameter(4, Types.INTEGER);

	cstmt.execute();
	int orderId = cstmt.getInt(4);

	if (orderId > 0) {

	    cstmt = dc.cn.prepareCall("{call proc_ChiTietHoaDon_Create(?, ?, ?, ?, ?)}");

	    cstmt.setString(1, bookId);
	    cstmt.setInt(2, quantity);
	    cstmt.setInt(3, orderId);
	    cstmt.setInt(4, 0);
	    cstmt.registerOutParameter(5, Types.INTEGER);

	    cstmt.execute();

	    int statusCode = cstmt.getInt(5);

	    if (statusCode > 0) {
		return true;
	    }
	}
	return false;
    }

    public LichSuMuaHangBo getOrderHistoty(int customerId) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_HoaDon_OrderHistory(?)}");

	cs.setInt(1, customerId);

	ResultSet rs = cs.executeQuery();

	LichSuMuaHangBo list = new LichSuMuaHangBo();

	while (rs.next()) {

	    LichSuMuaHangBean ls = new LichSuMuaHangBean();
	    
	    ls.setTenSach(rs.getString("tensach"));
	    ls.setAnh(rs.getString("anh"));
	    ls.setTongTien(rs.getLong("SoLuongMua") * rs.getLong("gia"));
	    ls.setTrangThai(rs.getInt("TrangThai"));
	    ls.setThoiGianMua(rs.getDate("NgayMua"));
	    ls.setMaSach(rs.getString("MaSach"));
	    ls.setMaHoaDon(rs.getString("MaHoaDon"));
	    
	    list.ThemObject(ls);
	}
	return list;
    }

    public int chuyenTrangThaiDatHang(String orderId, String ttOrder) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Tranforms_Cus(?, ?, ?)}");

	cs.setString(1, orderId);
	cs.setString(2, ttOrder);

	cs.registerOutParameter(3, Types.BIGINT);

	cs.execute();

	int result = cs.getInt(3);

	return result;
    }

    public boolean huyDatHang(String orderId) throws Exception {
	DungChung dc = new DungChung();
	dc.ketNoi();

	CallableStatement cs = dc.cn.prepareCall("{call proc_Order_Cancel_Cus(?, ?)}");

	cs.setString(1, orderId);

	cs.registerOutParameter(2, Types.BIT);

	cs.execute();

	boolean result = cs.getBoolean(2);

	return result;
    }
}
